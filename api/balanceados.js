/*
    Lista apenas os pokemons o mesmo valor de ataque e defesa
*/

var data = require('../data')

module.exports = function(req, res){

    //Implementação
    var result = data.pokemon.filter((pokemon) =>{
		return pokemon.attack == pokemon.defense; 
	});
	
    //Retorno
    res.json(result)
}