/*
    Lista o número de pokemons que iniciam com cada letra do alfabeto
*/

var data = require('../data')

module.exports = function(req, res){

    const pokemonsPorLetra = new Map();
    for(let i=65; i<=90; i++){ //Inicia o map com todas as letras zeradas
        const letra = String.fromCharCode(i);
        pokemonsPorLetra.set(letra, {letra, quantidade: 0});

    }

    data.pokemon.forEach((p) => pokemonsPorLetra.get(p.name[0].toUpperCase()).quantidade++);

    var result = [...pokemonsPorLetra.values()]; //Cria um array só com os values do map

    //Retorno
    res.json(result)
}